//http://enos.itcollege.ee/~jpoial/algorithms/examples/Astack.java
//https://github.com/paopao2/Algorithm-Practice/blob/master/Evaluate%20Reverse%20Polish%20Notation.java
//http://www.learn4master.com/interview-questions/leetcode/leetcode-evaluate-reverse-polish-notation-java
//https://rosettacode.org/wiki/Parsing/RPN_calculator_algorithm#Java_2
//http://stackoverflow.com/questions/549808/compare-objects-in-linkedlist-contains
//http://andreinc.net/2011/01/03/rpn-calculator-using-python-scala-and-java/
//http://stackoverflow.com/questions/8052941/throwing-a-custom-numberformatexception-in-java
//koodi loomisel olen abi saadud yhisoppest kaastudengitega

import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.List;

public class DoubleStack {
   private LinkedList<Double> loend1 = new LinkedList<Double>();
   
   
   public static void main (String[] argum) {
      // TODO!!! Your tests here!
	   //String s = "35. 10. + -";  
	   //String s = "35. y 10. -3. + - +";
	   //String s = "2.0 5.0 *";
	  
	// DoubleStack.interpret(s);  
   }
   
   DoubleStack() {
      // TODO!!! Your constructor here!
	  
   }
   /*DoubleStack(int suurus){ // jatan valja, sest kehtib magasini kohta.
	  loend1 = new double[suurus];
	  sp = -1;
   }*/
   @Override
   public Object clone() throws CloneNotSupportedException {
      //return this; // TODO!!! Your code here!
      DoubleStack tmp = new DoubleStack();
     
      for (int i= (loend1.size()-1); i >=0; i--) {
          tmp.push(loend1.get(i));
      }
      return tmp;
   }

   public boolean stEmpty() { //alataitumise kontroll
      return loend1.isEmpty(); // TODO!!! Your code here!
      
   }
   /*public boolean stOverflow() { //yletaitumise kontrolli LinkedListi puhul ei tee, kuna listi suurus on dŁnaamiline
		return ((sp + 1) >= loend1.length);
   }*/
   
   public void push (double a) {
      // TODO!!! Your code here!
	   // element lisatakse listi esimeseks
	   loend1.addFirst(a);
   }

   public double pop() throws RuntimeException {
      //return 0.; // TODO!!! Your code here!
      if (stEmpty())
			throw new RuntimeException("Loend on tyhi, ei saa elemente eemaldada.");
		double tipp =loend1.getFirst(); // leiab tipuelemendi
		loend1.removeFirst(); // eemaldab esimese
		return tipp; // valjastab tulemuse
   } // pop

   public void op (String s) throws ArithmeticException {
      // TODO!!!
	   
		      if(loend1.size() < 2)
		      {
		         throw new RuntimeException("Loendis on vahe elemente, ei saa operatsioone teha.");
		      }

	   double op2 = pop();
	   double op1 = pop();
	   if (s.equals ("+")) push (op1 + op2);
	   else if (s.equals ("-")) push (op1 - op2);
	   else if (s.equals ("*")) push (op1 * op2);
	   else if (s.equals ("/")) {
		   if (op2 !=0)
			   push (op1 / op2);
		   else 
	  			 throw new ArithmeticException ("Nulliga jagamine, tehet ei saa teostada!");
	   }
	   else
	   throw new RuntimeException("Sisestatud symbol [" + s + "] ei ole sobiv tehtemark +, -, /, *.");
   }
  
   public double tos() {
      //return 0.; // TODO!!! Your code here!
      if (stEmpty())
    	  throw new RuntimeException ("Loend on tyhi, tipuelement puudub.");
	  double tipp =loend1.getFirst();
      return tipp;
   }

   @Override
   public boolean equals (Object o) { // loendite vordlemine
      //return true; // TODO!!! Your code here!
	   
	      if (loend1.equals(((DoubleStack) o).loend1))
	      {
	         return true;
	      }else {
	         return false;
	      }
   }
   @Override
   public String toString() { //soneks teisendamine
      //return null; // TODO!!! Your code here!
      if (stEmpty())
			return "Loend on tyhi, ei saa teisendada.";
		StringBuffer b = new StringBuffer();
		for (int i = (loend1.size()-1); i >=0; i--)
			b.append(String.valueOf(loend1.get(i)));
		return b.toString();
   }

   public static double interpret (String pol) {
	 // return 0.; // TODO!!! Your code here!
	   if (pol == null || pol.length() == 0)
			throw new RuntimeException("RPN loendi sisend on tyhi.");

	   
	   	DoubleStack loend2 = new DoubleStack();
		StringTokenizer tk = new StringTokenizer(pol, " \t");

		int i = 1;
		int loendur = tk.countTokens();
			
		while (tk.hasMoreElements()) {
				
			String symbol = (String) tk.nextElement();
				
			if (symbol.equals("-") || symbol.equals("+") || symbol.equals("/") || symbol.equals("*")) {	
				if (loend2.loend1.size() == 0){
					throw new RuntimeException("RPN loendis [" + pol + "] ei ole elemente tehte teostamiseks"); 
				}
				
				else if (loend2.loend1.size() == 1){
					throw new RuntimeException("RPN loendis [" + pol + "] on alles yks element " + loend2.tos() + ", mis on liiga vahe viimase tehte \"" + symbol  + "\" teostamiseks");
				}else{                       
				loend2.op(symbol);
				}
		 	} else{
					if (loendur == i && i > 1) {
						throw new RuntimeException("RPN loendis [" + pol + "] on yleliigseid symboleid.");
					}
					try{
					    double a = Double.parseDouble(symbol);
					    loend2.push(a);
					}
					catch(RuntimeException e) {           
					    throw new RuntimeException("RPN loendis ["+ pol + "] olev symbol " + symbol +" ei sobi RPN meetodi teostamiseks."); 
					}  
				}
				i++;
			}
		//System.out.println("RPN loendi [" + pol + "] lahend on " + loend2);

		return loend2.tos();
		
      }
	  
   }



